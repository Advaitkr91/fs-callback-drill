const fs = require("fs");
const path = require("path");

function problem1(cb) {
  if (arguments.length !== 1 || typeof cb !== "function") {
    throw new Error("not sufficient data");
  }
  try {
    fs.mkdir(path.join(__dirname, "testfolder"), (err) => {
      if (err) {
        console.log("failed to create", err);
      }
      else
      {
        for (let item = 0; item < 5; item++) {
          fs.writeFile(
            path.join(__dirname, "testfolder", `${item}.json`),
            "utf-8",
            (err) => {
              if (err) {
                console.log(err);
              }
              else{
               console.log("file created")
              }
            })
        }
            setTimeout(()=>{
            for (let index = 0; index < 5; index++) {
                  fs.unlink(
                      path.join(__dirname, "testfolder", `${index}.json`),
                      function (err) {
                        if (err) {
                          console.log("error", Error);
                        }else{
                            
                            console.log("file deleted");
                            
                        }
                      }
                    );
                    console.log("file deleted");
          }
        },3000)
  
      }
        });
    
      } catch (error) {
    console.log("error", error);
  }

}

module.exports = problem1;
