const { throws } = require("assert");
const fs = require("fs");
const path = require("path");
function problem2(cb) {
  if (arguments.length !== 1 || typeof cb !== "function") {
    throw new Error("not sufficient data");
  }

  try {
  function filenameRead(filePath) {
    fs.readFile(path.join(filePath, "filenames.txt"),(err)=>{

      if(err){
           console.log(console.error("data not found"))
      }
      fs.writeFile(path.join(filePath, "filenames.txt"), '', function(){console.log('done')})
 })

  }

  function saveFile(filePath) {
          fs.writeFile(path.join(filePath,"filenames.txt"),"sort.txt",(err,data)=>{
              if(err){
                 console.log(err)
              }
              filenameRead(filePath)

          })
  }
   function createNewFileforsortText(filePath,finalString) {

          fs.writeFile(path.join(filePath,"sort.txt"),finalString,(err,data)=>{
                if(err){
                  throw new Error("no data")
                }
                else{


                      saveFile(filePath)
                }



          })


     
   }
   function sortlowerTextfile(filePath) {

       fs.readFile(path.join(filePath,"lower.txt"),(err,data)=>{

           if(err){
            throw new Error("no data")
           }
           else{
                let finalString = ""
                let newContent = data.toString()
                let str1 = newContent.toString().split(/[,.]/);
                for (let str of str1) {
                  let sentence = str.split(/\s/);
                  for (let word of sentence) {
                    let str = word.split("");
                    str.sort((a, b) => {
                      if (a > b) {
                        return 1;
                      }
                      if (b > a) {
                        return -1;
                      } else {
                        return 0;
                      }
                    });

                    finalString += str.join("") + " ";

                  }

                }
                createNewFileforsortText(filePath,finalString)  
           }
       })
      
   }



    function createNewFile(filePath,info) {
       fs.writeFile(path.join(filePath,"lower.txt"),info,(err)=>{
           if(err){

              throw new Error("no data")
           }
           else{
                  console.log("new file created")
                  sortlowerTextfile(filePath)
           }

       })    
            
    }

    function readNewFile(filePath) {
      fs.readFile(path.join(filePath, "upper.txt"), (err, data) => {
        if (err) {
          throw new Error("no data");
        }else{
             
               
                let info = data.toString().toLowerCase().split(/[,.]/)
                createNewFile(filePath,info)
           
        }

      })
      
    }
    
    function write2(filePath,content){
      fs.writeFile(path.join(filePath,"upper.txt"),content,
        (err,data) => {
          if (err) {
            throw new Error("no data");
          }
          else{

               readNewFile(filePath)

          }

        })

       
    }
    
      
    
      
     function read(filePath){
      fs.readFile(path.join(filePath,"lipsum.txt"),"utf-8",(err,data)=>{

           if(err){
              console.log(err)
           }
           else{
            
                let content = data.toString().toUpperCase()
               console.log(content)
               write2(filePath,content)
           }
      })

     }

     function final(){
      const filePath = path.join(__dirname, "data");
      read(filePath)
       
     }
    final() 
    /*const filePath = path.join(__dirname, "data");
    fs.readFile(path.join(filePath, "lipsum.txt"), "utf-8", (err, data) => {
      if (err) {
        throw new Error("not able to send data");
      }
      fs.writeFile(
        path.join(filePath, "upper.txt"),
        data.toString().toUpperCase(),
        (err, data) => {
          if (err) {
            throw new Error("not able to right data");
          }
          fs.writeFile(
            path.join(filePath, "filenames.txt"),
            "upper.txt",
            (err, data) => {
              if (err) {
                throw new Error("no data");
              }
              fs.readFile(path.join(filePath, "upper.txt"), (err, data) => {
                if (err) {
                  throw new Error("no data");
                }
                //data.toString().toLowerCase().split(".")
                fs.writeFile(
                  path.join(filePath, "lower.txt"),
                  data.toString().toLowerCase().split("."),
                  (err, data) => {
                    if (err) {
                      throw new Error("no input");
                    }

                    fs.writeFile(
                      path.join(filePath, "filenames.txt"),
                      "lower.txt",
                      (err, data) => {
                        if (err) {
                          throw new Error("not added to filenames.txt");
                        }
                        fs.readFile(
                          path.join(filePath, "lower.txt"),
                          (err, data) => {
                            if (err) {
                              throw new Error("no data read");
                            }
                            //console.log(data.toString())
                            let finalString = " ";
                            let str1 = data.toString().split(/[,.]/);
                            for (let str of str1) {
                              let sentence = str.split(/\s/);
                              for (let word of sentence) {
                                let str = word.split("");
                                str.sort((a, b) => {
                                  if (a > b) {
                                    return 1;
                                  }
                                  if (b > a) {
                                    return -1;
                                  } else {
                                    return 0;
                                  }
                                });

                                finalString += str.join("") + " ";
                              }
                            }
                            fs.writeFile(
                              path.join(filePath, "sort.txt"),
                              finalString,
                              (err, data) => {
                                if (err) {
                                  throw new Error("not added to filenames.txt");
                                } else {
                                  fs.writeFile(
                                    path.join(filePath, "filenames.txt"),
                                    "sort.txt",
                                    (err, data) => {
                                      if (err) {
                                        throw new Error(
                                          "not added to filenames.txt"
                                        );
                                      }
                                      else{
                                         fs.readFile(path.join(filePath, "filenames.txt"),(err)=>{

                                              if(err){
                                                   console.log(console.error("data not found"))
                                              }
                                              fs.writeFile(path.join(filePath, "filenames.txt"), '', function(){console.log('done')})
                                         })


                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        );
                      }
                    );
                  }
                );
              });
            }
          );
        }
      );
    });
    */
  } catch (error) {
    console.log("error", error);
  }
}
module.exports = problem2;
